/* eslint-disable no-underscore-dangle */
const jwt = require('jsonwebtoken');
const secret = 'oUF2M0OZVtHi0qmdEHubdYJebmtwADuL';

const failed = {
    status: 405,
    message: 'User needs to be logged-in for this action',
};

const extractToken = (bearerToken) => bearerToken.split(' ')[1];

module.exports.isSelfOrAdmin = (data) => {
    let userId = data.params?.userId ?? null;
    if (userId === null) userId = data.body?.userId ?? -1;

    let allowed = data.accessorId === userId;
    if (allowed === false) allowed = data.isAdmin === true;

    return allowed;
};

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };

    return jwt.sign(data, secret, {});
};

module.exports.verify = (request, response, next) => {
    const bearerToken = request.headers.authorization;
    if (typeof bearerToken !== 'undefined') {
        const token = extractToken(bearerToken);
        // eslint-disable-next-line consistent-return
        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return response.status(failed.status).send(failed.message);
            }
            next();
        });
    }

    return response.status(failed.status).send(failed.message);
};

module.exports.decode = (bearerToken) => {
    if (typeof bearerToken !== 'undefined') {
        const token = extractToken(bearerToken);
        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return response.status(failed.status).send(failed.message);
            }
            return jwt.decode(token, { complete: true }).payload;
        });
    }

    return response.status(failed.status).send(failed.message);
};
