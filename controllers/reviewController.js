/* eslint-disable no-underscore-dangle */
/* eslint-disable consistent-return */
const bcrypt = require('bcrypt');

const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Review = require('../models/Review.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [R] Review
// ----- [R/C] Create
// ----- [R/RA] Retrieve All
// ----- [R/R] Retrieve
// ----- [R/R] Retrieve By User and Product
// ----- [R/RPR] Retrieve Product Reviews
// ----- [R/RUR] Retrieve User Reviews
// ----- [R/U] Update
// [RU] Review Utilities
// ----- [RU/CR] Compute Rating for Product
// ----------------------------- //
// ----------------------------- //

// ----------------------- //
// ----- [R] Reviews ----- //
// ----------------------- //

// ----- [R/C] Create ----- //
module.exports.create = async (data) => {
    if (data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    const reviewExist = await Review.find({
        userId: data.accessorId,
        productId: data.body.productId,
    }).then((result) => (result.length > 0));

    if (reviewExist) {
        return {
            status: 405,
            data: {
                status: 405,
                message: 'User had already made a review for this product',
            },
        };
    }

    let product = null;
    try {
        product = await Product.findOne({ _id: data.body.productId }).then((result, error) => {
            if (error) {
                return null;
            }

            return {
                id: result._id,
                rating: result.rating,
            };
        });
    } catch {
        product = null;
    }

    if (product === null) {
        return {
            status: 404,
            data: {
                status: 404,
                message: `Product with this id:${data.body.productId} cannot be found`,
            },
        };
    }

    const newReview = new Review({
        userId: data.accessorId,
        productId: product.id,
        rating: data.body.rating,
        comment: data.body.comment ?? '',
    });

    const review = await newReview.save().then((result, error) => {
        if (error) {
            return null;
        }
        return result;
    });
    const newProductRating = await computeProductRating(product.id);

    if (review && newProductRating) {
        return {
            status: 201,
            data: {
                status: 201,
                message: 'User successfully created a review',
                newProductRating,
                review,
            },
        };
    }
};

// ----- [R/RA] Retrieve All ----- //
module.exports.retrieveAll = () => Review.find({}).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve reviews at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            reviews: result,
        },
    };
});

// ----- [R/R] Retrieve ----- //
module.exports.retrieve = (data) => Review.findOne({ _id: data.params.reviewId }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Unable to retrieve review at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            review: result,
        },
    };
}).catch((error) => ({
    status: 404,
    data: {
        status: 404,
        message: `Review with this id:${error.value} cannot be found`,
    },
}));

// ----- [R/R] Retrieve By User and Product ----- //
module.exports.retrieveByUserAndProduct = (data) => Review.findOne({
    productId: data.params.productId,
    userId: data.params.userId,
}).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Unable to retrieve review at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            review: result,
        },
    };
}).catch((error) => ({
    status: 404,
    data: {
        status: 404,
        message: 'Review cannot be found',
    },
}));

// ----- [R/RPR] Retrieve Product Reviews ----- //
module.exports.retrieveAllByProduct = (data) => Review.find({
    productId: data.params.productId,
}).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve reviews at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            reviews: result,
        },
    };
});

// ----- [R/RUR] Retrieve User Reviews ----- //
module.exports.retrieveAllByUser = (data) => Review.find({ userId: data.params.userId }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve reviews at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            reviews: result,
        },
    };
});

// ----- [R/U] Update ----- //
module.exports.update = async (data) => {
    if (data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    let byAccessor = false;
    try {
        byAccessor = await Review.findOne({ _id: data.params.reviewId }).then((result, error) => {
            if (error) {
                return false;
            }

            return data.accessorId === result.userId;
        });
    } catch {
        byAccessor = false;
    }

    if (byAccessor === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    const review = await Review.findByIdAndUpdate(data.params.reviewId, {
        rating: data.body.rating,
        comment: data.body.comment ?? '',
        updatedOn: new Date(),
    }, {
        returnDocument: 'after',
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return null;
        }
        return result;
    }).catch((error) => null);
    const newProductRating = await computeProductRating(review.productId);

    if (review && newProductRating) {
        return {
            status: 200,
            data: {
                status: 200,
                message: 'User successfully updated the review',
                newProductRating,
                review,
            },
        };
    }
};

// -------------------------------- //
// ----- [RU] Review Utilities ----- //
// -------------------------------- //

// ----- [RU/CR] Compute Rating for Product
const computeProductRating = async (productId) => {
    const productReviews = await Review.find({ productId }).then((results) => results);
    let productRatingSum = 0;
    let productRatingCount = 0;
    productReviews.forEach((result) => {
        productRatingSum += result.rating;
        productRatingCount += 1;
    });
    const productRating = (productRatingSum / productRatingCount);

    return Product.findByIdAndUpdate(productId, {
        rating: productRating,
        updatedOn: new Date(),
    }, {
        returnDocument: 'after',
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return 'Error while computing new rating';
        }
        return result.rating;
    }).catch((error) => 'Error while computing new rating');
};
