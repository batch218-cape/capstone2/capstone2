/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
const bcrypt = require('bcrypt');

const User = require('../models/User.js');
const Product = require('../models/Product.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [UA] User Authentication
// ----- [UA/R] Register
// ----- [UA/L] Login
// [UD] User Details
// ----- [UA/RAU] Retrieve All Users
// ----- [UA/RU] Retrieve User
// ----- [UD/CP] Change Password
// ----- [UD/SA] Set Admin
// [C] Cart
// ----- [C/RC] Retrieve Cart
// ----- [C/AC] Add to Cart
// ----- [C/UCI] Update Cart Item
// ----- [C/EC] Empty Cart
// ----------------------------- //
// ----------------------------- //

// ------------------------------------ //
// ----- [UA] User Authentication ----- //
// ------------------------------------ //

// ----- [UA/R] Register ----- //
module.exports.register = async (data) => {
    const emailExist = await User.find({ email: data.body.email }).then((result) => (result.length > 0));

    if (emailExist === true) {
        return {
            status: 405,
            data: {
                status: 405,
                message: 'Email has already been registered. Please login instead',
            },
        };
    }

    if (data.body.password.length <= 7) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Password must atleast be 8 characters long',
            },
        };
    }

    const newUser = new User({
        firstname: data.body.firstname,
        lastname: data.body.lastname,
        email: data.body.email,
        password: bcrypt.hashSync(data.body.password, 10),
        mobile: data.body.mobile,
        address: data.body.address,
    });

    return newUser.save().then((user, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to register user at this time',
                },
            };
        }

        return {
            status: 201,
            data: {
                status: 201,
                message: 'User has been registered',
            },
        };
    });
};

// ----- [UA/L] Login ----- //
module.exports.login = (data) => User.findOne({ email: data.body.email }).then((result) => {
    if (result === null) {
        return {
            status: 404,
            data: {
                status: 404,
                message: 'Email has not been registered with',
            },
        };
    }

    const isPasswordCorrect = bcrypt.compareSync(data.body.password, result.password);
    if (isPasswordCorrect) {
        return {
            status: 200,
            data: {
                status: 200,
                id: result._id,
                access: auth.createAccessToken(result),
                message: 'Successfully logged-in',
            },
        };
    }

    return {
        status: 400,
        data: {
            status: 400,
            message: 'Incorrect password. Please try again',
        },
    };
});

// -------------------------- //
// ----- [UD] User Data ----- //
// -------------------------- //

// ----- [UD/RAU] Retrieve All Users ----- //
module.exports.retrieveAll = async (data) => {
    if (auth.isSelfOrAdmin(data) === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return User.find({}).select(['firstname', 'lastname', 'email', 'mobile', 'isAdmin']).sort({ lastname: 'asc', firstname: 'asc' }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Failed to retrieve users at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                users: result,
            },
        };
    });
};

// ----- [UD/RU] Retrieve User ----- //
module.exports.retrieve = async (data) => {
    if (auth.isSelfOrAdmin(data) === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return User.findOne({ _id: data.params.userId }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to retrieve user at this time',
                },
            };
        }

        // eslint-disable-next-line no-param-reassign
        result.password = '********';
        return {
            status: 200,
            data: {
                status: 200,
                user: result,
            },
        };
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// ----- [UD/CP] Change Password ----- //
module.exports.changePassword = async (data) => {
    if (auth.isSelfOrAdmin(data) === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    if (data.body.password.length <= 7) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Password must atleast be 8 characters long',
            },
        };
    }

    return User.findByIdAndUpdate(data.params.userId, {
        password: bcrypt.hashSync(data.body.password, 10),
    }, {
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to retrieve user at this time',
                },
            };
        }

        // eslint-disable-next-line no-param-reassign
        result.password = '********';
        return {
            status: 200,
            data: {
                status: 200,
                user: result,
                message: `Password has been updated for user: ${result.firstname} ${result.lastname}`,
            },
        };
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// ----- [UD/SA] Set Admin -----//
module.exports.setAdmin = async (data, newStatus) => {
    if (data.isAdmin === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    if (data.accessorId === data.params.userId && newStatus === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'Cannot set self as user',
            },
        };
    }

    return User.findByIdAndUpdate(data.params.userId, {
        isAdmin: newStatus,
        updatedOn: new Date(),
    }, {
        returnDocument: 'after',
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to set user\'s admin status at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                message: `${result.firstname} ${result.lastname}'s status is now set to ${newStatus ? 'admin' : 'user'}`,
                product: {
                    _id: result._id,
                    firstname: result.firstname,
                    lastname: result.lastname,
                    email: result.email,
                    isAdmin: result.isAdmin,
                },
            },
        };
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// -------------------- //
// ----- [C] Cart ----- //
// -------------------- //

// ----- [C/RC] Retrieve Cart ----- //
module.exports.retrieveCart = async (data) => {
    if (auth.isSelfOrAdmin(data) === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return User.findOne({ _id: data.params.userId }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to retrieve product at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                customer: {
                    firstname: result.firstname,
                    lastname: result.lastname,
                },
                cart: result.cart,
            },
        };
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// ----- [C/AC] Add to Cart ----- //
module.exports.addToCart = async (data) => {
    if (data.accessorId !== data.params.userId || data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    const { productId, quantity } = data.body;
    let isProductFound = false;
    try {
        isProductFound = await Product.findOne({ _id: productId }).then((result, error) => (!error));
    } catch {
        isProductFound = false;
    }

    if (isProductFound === false) {
        return {
            status: 404,
            data: {
                status: 404,
                message: `Product with this id:${productId} cannot be found`,
            },
        };
    }

    return User.findOne({ _id: data.params.userId }).then((result, error) => {
        let itemFound = false;
        result.cart.forEach((item) => {
            if (item.productId === productId) {
                if (quantity > 0) {
                    item.quantity += quantity;
                }
                itemFound = true;
            }
        });
        if (itemFound === false) {
            result.cart.push({
                productId,
                quantity: Number(quantity),
            });
        }
        result.updatedOn = new Date();

        return result.save().then((user, error) => {
            if (error) {
                return {
                    status: 400,
                    data: {
                        status: 400,
                        message: 'Unable to update cart at this time',
                    },
                };
            }

            return {
                status: 200,
                data: {
                    status: 200,
                    customer: {
                        firstname: result.firstname,
                        lastname: result.lastname,
                    },
                    cart: result.cart,
                },
            };
        });
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// ----- [C/UCI] Update Cart Item ----- //
module.exports.updateCartItem = async (data) => {
    if (data.accessorId !== data.params.userId || data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    const { productId, quantity } = data.body;
    let isProductFound = false;
    try {
        isProductFound = await Product.findOne({ _id: productId }).then((result, error) => (!error));
    } catch {
        isProductFound = false;
    }

    if (isProductFound === false) {
        return {
            status: 404,
            data: {
                status: 404,
                message: `Product with this id:${productId} cannot be found`,
            },
        };
    }

    return User.findOne({ _id: data.params.userId }).then((result, error) => {
        let itemFound = false;
        if (quantity > 0) {
            result.cart.forEach((item) => {
                if (item.productId === productId) {
                    item.quantity = quantity;
                    itemFound = true;
                }
            });

            if (itemFound === false) {
                result.cart.push({
                    productId,
                    quantity: Number(quantity),
                });
            }
        } else {
            let targetIndex = -1;
            for (let index = 0; index < result.cart.length; index += 1) {
                if (result.cart[index].productId === productId) {
                    targetIndex = index;
                    break;
                }
            }

            if (targetIndex > -1) {
                result.cart.splice(targetIndex, 1);
            }
        }
        result.updatedOn = new Date();

        return result.save().then((user, error) => {
            if (error) {
                return {
                    status: 400,
                    data: {
                        status: 400,
                        message: 'Unable to update cart at this time',
                    },
                };
            }

            return {
                status: 200,
                data: {
                    status: 200,
                    customer: {
                        firstname: result.firstname,
                        lastname: result.lastname,
                    },
                    cart: result.cart,
                },
            };
        });
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};

// ----- [C/EC] Empty Cart ----- //
module.exports.emptyCart = async (data) => {
    if (data.accessorId !== data.params.userId || data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return User.findOne({ _id: data.params.userId }).then((result, error) => {
        result.cart = [];
        result.updatedOn = new Date();

        return result.save().then((user, error) => {
            if (error) {
                return {
                    status: 400,
                    data: {
                        status: 400,
                        message: 'Unable to update cart at this time',
                    },
                };
            }

            return {
                status: 200,
                data: {
                    status: 200,
                    customer: {
                        firstname: result.firstname,
                        lastname: result.lastname,
                    },
                    cart: result.cart,
                },
            };
        });
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `User with this id:${error.value} cannot be found`,
        },
    }));
};
