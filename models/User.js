const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, 'First name is required'],
    },
    lastname: {
        type: String,
        required: [true, 'Last name is required'],
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        // eslint-disable-next-line no-useless-escape
        validate: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minlength: 8,
    },
    mobile: {
        type: String,
        required: [true, 'Mobile is required'],
        validate: /[0-9+#-]/,
    },
    address: {
        type: String,
        required: false,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    cart: [
        {
            productId: {
                type: String,
                required: [true, 'Product ID is required'],
            },
            quantity: {
                type: Number,
                required: [true, 'Quantity is required'],
            },
        },
    ],
    productsBought: [
        {
            type: String,
            required: [true, 'Product ID is required'],
        },
    ],
    createdOn: {
        type: Date,
        default: new Date(),
    },
    updatedOn: {
        type: Date,
        default: new Date(),
    },
});

module.exports = mongoose.model('User', userSchema);
