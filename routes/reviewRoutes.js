const express = require('express');
const router = express.Router();

const reviewController = require('../controllers/reviewController.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [R] Review
// ----- [R/C] Create
// ----- [R/RA] Retrieve All
// ----- [R/R] Retrieve
// ----- [R/R] Retrieve By User and Product
// ----- [R/RPR] Retrieve Product Reviews
// ----- [R/RUR] Retrieve User Reviews
// ----- [R/U] Update
// ----------------------------- //
// ----------------------------- //

// ----------------------- //
// ----- [R] Reviews ----- //
// ----------------------- //

// ----- [R/C] Create ----- //
router.post('/create', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    reviewController.create(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/RA] Retrieve All ----- //
router.get('/', (req, res) => {
    reviewController.retrieveAll().then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/R] Retrieve ----- //
router.get('/:reviewId', (req, res) => {
    const data = {
        params: req.params,
    };

    reviewController.retrieve(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/R] Retrieve By User and Product ----- //
router.get('/user/:userId/product/:productId', (req, res) => {
    const data = {
        params: req.params,
    };

    reviewController.retrieveByUserAndProduct(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/RPR] Retrieve Product Reviews ----- //
router.get('/product/:productId', (req, res) => {
    const data = {
        params: req.params,
    };

    reviewController.retrieveAllByProduct(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/RUR] Retrieve User Reviews ----- //
router.get('/user/:userId', (req, res) => {
    const data = {
        params: req.params,
    };

    reviewController.retrieveAllByUser(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [R/U] Update ----- //
router.patch('/:reviewId/update', (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    reviewController.update(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

module.exports = router;
